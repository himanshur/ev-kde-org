---
title: 'KDE e.V. Report for 2016'
date: 2017-05-04 00:00:00 
layout: post
---

KDE e.V., the non-profit organisation supporting the KDE community, is happy to present the <a href="https://ev.kde.org/reports/ev-2016/" target="_blank">report for 2016</a>.

This report includes statements from the board, a featured article about the 20th anniversary of KDE, summaries about developer sprints and conferences supported by KDE e.V., statements from our Working Groups, development highlights for 2016, and some information about the current structure of KDE e.V.
      
