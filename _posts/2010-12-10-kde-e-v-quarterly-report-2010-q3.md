---
title: 'KDE e.V. Quarterly Report 2010 Q3'
date: 2010-12-10 00:00:00 
layout: post
---

The <a href="http://ev.kde.org/reports/ev-quarterly-2010Q3.pdf">KDE e.V.
Quarterly Report</a> is now available for the third quarter of 2010, from
July to September. This document includes reports of the board and the working
groups about KDE e.V. activities, reports from sprints, financial information,
and more.

