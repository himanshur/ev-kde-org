---
title: "General Assembly KDE e.V. 2020"
layout: page
---

# Mitgliederversammlung KDE e.V. 2020

Die Mitgliederversammlung findet am Montag, den 7. September 2020 um 13:00 CEST statt.

Die Versammlung wird online in einer passwortgeschützten Videokonferenz mit integriertem Chat durchgeführt. Das verwendete System ist das Open-Source-Tool [BigBlueButton](https://bigbluebutton.org/). Tagesordnung und Präsentationen werden über das Open-Source-Tool [OpenSlides](https://openslides.com/de) verwaltet und geteilt. In OpenSlides wird auch die Anwesenheit der Mitglieder registriert und es werden die Vertreter-Stimmen verwaltet. Die Liste der Teilnehmer wird zur Dokumentation aus OpenSlides exportiert. Es sind 94 Mitglieder anwesend, darunter zwei nicht stimmberechtigte Fördermitglieder. Drei nicht anwesende Mitglieder haben Vertreter benannt. Es sind keine Nicht-Mitglieder anwesend.

Beide Systeme werden auf KDE-Servern betrieben. Alle Mitglieder haben im Vorfeld der Versammlung Zugriff auf die Tools erhalten.

## Tagesordnung

1. Begrüßung
2. Wahl des Versammlungsleiters
3. Bericht des Vorstands
   1. Bericht über Aktivitäten
   2. Bericht des Schatzmeisters
   3. Bericht der Rechnungsprüfer
   4. Entlastung des Vorstandes
4. Bericht der Vertreter und Arbeitsgruppen des Vereins
   1. Bericht der Vertreter in der KDE Free Qt Foundation
   2. Bericht der System Administration Working Group
   3. Bericht der Community Working Group
   4. Bericht der Financial Working Group
   5. Bericht der Advisory Board Working Group
   6. Bericht der Fundraising Working Group
5. Wahl des Vorstands
6. Wahl der Rechnungsprüfer
7. Wahl der Vertreter in der KDE Free Qt Foundation
8. Verschiedenes

## Protokoll

### Begrüßung und Wahl des Versammlungsleiters

Um 13:00 eröffnet der Vorsitzende des Vorstands, Aleix Pol, die Versammlung. Er bedankt sich bei den Organisatoren der Mitgliederversammlung.

Frederik Gladhorn wird per Akklamation zum Versammlungsleiter gewählt.

Der Versammlungsleiter stellt fest, dass die Einladung zur Mitgliederversammlung ordnungsgemäß und fristgemäß erfolgt ist. Es gibt keine Einwände.

Die Tagesordnung wird per Akklamation bestätigt.

Der Versammlungsleiter benennt Cornelius Schumacher als Protokollanten.

Der Versammlungsleiter stellt fest, dass das satzungsgemäße Quorum erfüllt ist.

### Bericht des Vorstands

Der Vorstand berichtet über die Aktivitäten des Vereins seit der letzten Mitgliederversammlung. Der Bericht wurde auch schon am 5.9.2020 auf der durch den Verein organisierten Konferenz [Akademy](https://akademy.kde.org/2020) vorgestellt.

Der aktuelle Vorstand besteht aus Aleix Pol i Gonzàlez (Vorsitzender), Eike Hein (Schatzmeister und Stellvertreter des Vorsitzenden), Lydia Pintscher (Stellvertreterin des Vorsitzenden), Adriaan de Groot und Neofytos Kolokotronis.

Die Amtszeit von Aleix, Lydia und Eike läuft mit dieser Mitgliederversammlung aus.

11 neue aktive Mitglieder sind dem Verein seit der letzten Mitgliederversammlung beigetreten.

Der Verein hat 84 Fördermitglieder im Vergleich zu 95 im letzten Jahr. Die Arbeit an der Infrastruktur für die Verwaltung der Fördermitglieder wird fortgesetzt. Ein neuer Berater wurde zu diesem Zweck beauftragt.

Ein neues Firmen-Fördermitglied ist beigetreten. Ein Firmen-Fördermitglied hat seine Mitgliedschaft beendet, da die Firma übernommen wurde und sich ihre Ausrichtung geändert hat.

Das Advisory Board wurde erweitert, es besteht aus Vertretern der KDE-Patrone und ausgewählten Community-Partnern.

Der Verein hat eine Angestellte und vier freie Mitarbeiter. Petra Gillert ist als Assistenz des Vorstands angestellt, Aniqa Khokhar und Paul Brown arbeiten als Marketing-Berater, Adam Szopa als Projekt-Koordinator und Allyson Alexandrou als Event-Koordinator.

Der Community-Bericht 2019 über Aktivitäten der Community und des Vereins wird vorgestellt.

Evolution von KDE: Die zweite Runde der Community-Ziele ist gerade aktiv. Weitere Strukturierung, Dokumentation und Unterstützung des Zielsetzungs- und Umsetzungs-Prozesses durch den Verein findet statt, untersützt durch Adam Szopa.

Ziele aus der ersten Runde der Ziele werden weiter unterstützt.

Es wurden die Rollen und Verantwortlichkeiten der Arbeitsgruppen des Vereins in einem systematischen Prozess in Zusammenarbeit des Vorstandes mit den Arbeitsgruppen geklärt. Die Ergebnisse wurden den Mitgliedern bereits vorgestellt und werden nach der Vereins-Konferenz veröffentlicht.

Es gibt einige Ideen, die Satzung des Vereins anzupassen und weiterzuentwickeln. Der Vorstand sammelt mögliche Verbesserungen. Der Prozess befindet sich in einer frühen Phase und soll sorgfältig und ohne Eile durchgeführt werden. Der Vorstand bittet um Sammlung der Vorschläge für Änderungen.

KDE feiert am 14. Oktober 2021 seinen 25. Geburtstag. Der Vorstand bittet um Ideen, wie das Jubiläum genutzt und gefeiert werden kann.

#### Ziele des Vorstands vom letzten Jahr

* Akademy 2020 ausrichten: Die Veranstaltung findet statt, wegen der Corona-Situation als virtueller Event. 600 Teilnehmer haben sich angemeldet. Die Veranstaltung wird als Erfolg gewertet.
* Investition in die Entwicklung unsere Community-Mitglieder: Mehrere Trainings haben stattgefunden, z.B. ein Training zu impliziter Voreingenommenheit. Die Reichweite von Trainings soll weiter erhöht werden. Das Angebot von Zugang zu professionellen Bibliotheken fand keine Resonanz. Ideen für zukünftige Aktivitäten sind willkommen.
* Klarheit über die Rolle von KDE in ökologischer Nachhaltigkeit: Reisen haben den größten direkten Umwelteinfluss. Durch die Corona-Situation sind sie stark zurückgegangen. Eine andere Richtung wird verfolgt durch ein Projekt im Kontext des Blauen Engels.
* Effektivität des Netzwerks des Vorstands: Klärung der Rollen und Verantwortlichkeiten der Arbeitsgruppen wurde abgeschlossen.
* Bewusste und effektive Ausgabe von Mitteln: In zwei Bereichen wurden freie Mitarbeiter beauftragt und es wurde in Infrastruktur für virtuelle Events investiert.
* Schaffung von Prozessen: Prozesse wurden verbessert, professionalisiert und dokumentiert, zum Beispiel der Einführungsprozess für neue Mitarbeiter.
* Fördern von Partnerschaften mit anderen nahestehenden Organisationen: Der Linux App Summit wurde erfolgreich in Zusammenarbeit mit dem GNOME-Projekt durchgeführt.

Zwei Drittel der Ziele wurden erreicht, andere Ziele sind durch Änderung der Rahmenbedingungen überholt worden.

#### Schlüsselthemen für das nächste Jahr

* Wieder zusammen kommen: Persönliche Treffen werden wieder belebt, sofern die Gesundheitsregelungen das zulassen. Es wird einen Aufruf für Veranstalter der Akademy 2021 geben.
* Nachhaltige Finanzierung: Die Infrastruktur für Fördermitglieder wird verbessert und die Attraktivität des Fördermitgliedsprogramms wird verbessert.
* Nachhaltige Arbeit: Es sollen Fortschritte gemacht werden, um mehr Menschen zu ermöglichen, von KDE-bezogener Arbeit zu leben.
* Unterstützung eines Open-Source-freundlichen Hardware-Ökosystems: Das Partner-Netzwerk wird ausgebaut, um Open-Source-freundliche Hardware zu fördern.
* Kopieren und verstärken was funktioniert: Es werden Gebiete von KDE identifiziert, die besonders erfolgreich sind, und daraus gewonnene Erkenntnisse werden auch in anderen Bereichen des KDE e.V. angewendet.

### Bericht des Schatzmeisters

Eike Hein gibt den Bericht des Schatzmeisters, der mit der Hilfe der Financial Working Group und des Vorstands erstellt wurde.

#### Finanzjahr 2019

Die Einnahmen im Jahr 2019 waren stabil. Es gab keine Spenden-Kampagnen, da es zwei sehr große Spenden im Jahr davor gab, vom Pineapple Fund und der Handshake Foundation. Trotzdem ist die Einnahmen-Situation sehr stabil. Eike betont, dass das eine gute Situation für den Verein ist.

Einige Firmenförder-Mitgliedsbeiträge für das Jahr 2019 haben sich ins Jahr 2020 verschoben.

Das Sponsoring von Akademy entwickelt sich sehr positiv. Die Veranstaltung ist aus finanzieller Sicht stabil.

Es gab höhere Ausgaben als Einnahmen, wie geplant, um der Verpflichtung nachzukommen, Einnahmen gemäß des Vereinszwecks zu verwenden und die hohen Spenden des letzten Jahres einzusetzen. Es war geplant, noch höhere Ausgaben zu tätigen. Dieses Ziel wurde nicht erreicht, da zum Beispiel Mittel für die Gruppen aus dem Community-Ziele-Prozess nicht abgerufen wurden.

Events stellen einen großen Anteil in dern Ausgaben dar. Mit dem Linux App Summit wurde ein neuer großer Event finanziert. Die Treffen in Randa waren letztes Jahr noch im Plan, werden dieses Jahr durch den Linux App Summit ersetzt. Ein weiterer Summit ist geplant.

Die Ausgaben sind kontinuierlich gewachsen in den letzten Jahren.

Die Buchhaltung und finanzielle Auswertung gestaltete sich aus verschiedenen Gründen etwas problematischer aufgrund eines Personalwechsels beim Steuerberater. Es wurden Schritte unternommen die Situation zu verbessern.

Bei den Einnahmen gibt es weitgehende Stabilität, insbesondere was die Firmen-Fördermitglieder angeht. Insgesamt machen Fördermitglieder den Großteil der Einnahmen aus.

Bei den Ausgaben machen Mitarbeiter, Akademy und andere Veranstaltungen drei Viertel der Ausgaben aus.

#### Finanzplan 2020

Der Budget-Plan für 2020 wurde erst spät im Juni veröffentlicht, da er Anpassung durch die Corona-Krise benötigte. Der Schwerpunkt wurde von Unterstützung von Reisen zur Finanzierung von Mitarbeitern verschoben. Außerdem wurde der Aufbau von Infrastruktur für virtuelle Events hinzugefügt.

Die Gesamt-Strategie des Plans, die Unterstützung für zentrale Bereiche der Community zu erhöhen hat sich nicht geändert. Die Liquidität ist sehr gut. Reserven haben sich erhöht. Der Barbestand beträgt momentan 650.000 EUR, davon ist ein Teil zweckgebunden.

Dem Community-Ziele-Team steht ein Budget zu Verfügung. Das ist eine zielführende Art des Mitteleinsatzes, da das Team ein klares Mandat der Community hat.

#### Aktueller Stand 2020

Spendeneinnahmen sind weiterhin stabil. Ein neues Firmen-Fördermitglied (Kontent GmbH) ist dem Verein beigetreten, ein anderes Firmen-Fördermitglied hat als KDE-Patron den Verein verlassen.

Akademy ist profitabel, trotz niedrigerer Sponsorenlevels wegen des virtuellen Events. Der Fokus von Akademy liegt generell auf der Qualität der Veranstaltung für die Community, nicht auf Erzielung eines Überschusses.

Die Ausgaben für Mitarbeiter wurden merklich erhöht.

Die Ausgaben für Reisekosten werden in Zukunft deutlich steigen, wenn Reisen wieder in größerem Umfang möglich ist.

Die Reserven erlauben, für einige Jahren die Ausweitung der Aktivitäten weiter zu betreiben. Es ist wichtig, damit auch eine nachhaltige Finanzierung für die Aktivitäten aufzubauen. Die Arbeit in der Fund Raising Working Group und in anderen Teilen der Community ist dafür besonders wichtig. Es stehen etwa drei Jahre zur Verfügung, um das Einkommen zu steigern und belastbare Pläne zu entwickeln. Dabei sollen auch Dinge ausprobiert werden und daraus gelernt werden. Der Vorstand hat vor, das in der nächsten Amtszeit anzugehen.

### Bericht der Kassenprüfer

Die Kassenprüfer Andreas Cord-Landwehr und Ingo Klöcker berichten.

Die Mitglieder haben den Bericht der Kassenprüfer schon im Vorfeld per Email erhalten.

Die Kassenprüfer haben die Prüfung bei einem Treffen im KDE-Büro mit Lydia und Petra durchgeführt und haben Einsicht in die Bücher genommen.

Die Buchführung war von hoher Qualität, sogar noch höher als letztes Jahr. Es wurden nur bei sehr eingehender Betrachtung einige nicht wesentliche Feststellungen gemacht. Die Kassenprüfer sind mit der Buchführung sehr zufrieden.

Die Kassenprüfer empfehlen die Entlastung des Vorstands für das Finanzjahr 2019.

### Entlastung des Vorstands

Der Versammlungsleiter erklärt die Bedeutung der Entlastung des Vorstands.

Der Versammlungsleiter bittet um Meldung zu Fragen und Bedenken gegenüber der Entlastung. Es gibt keine Meldungen.

Die Entlastung wird über eine Umfrage im Online-Tool BigBlueButton durchgeführt. Der Vorstand nimmt nicht an der Umfrage teil.

Das Ergebnis sind 77 Ja-Stimmen für die Entlastung, keine Gegenstimmen und 8 Enthaltungen.

Der Versammlungsleiter bittet um Meldung bei Bedenken gegenüber der Abstimmung. Es gibt keine Meldungen.

Damit ist der Vorstand entlastet.

### Weitere Berichte

Die Berichte der Arbeitsgruppen und Vertreter wurden auf der Akademy schon öffentlich vorgestellt und im Vorfeld per Email allen Mitgliedern zur Verfügung gestellt.

#### Bericht der Vertreter in der KDE Free Qt Foundation

Olaf Schmidt-Wischhöfer und Martin Konold geben den Bericht als Vertreter des KDE e.V. in der KDE Free Qt Foundation.

Es finden weiterhin Verhandlungen mit The Qt Company über die Weiterentwicklung des Agreements zur Sicherstellung der Verfügbarkeit von Qt für freie Software-Entwicklung statt.

Ein Thema ist die teilweise Inkompatibilität der Bedingungen der kommerziellen Lizenz und der Verwendung als Open-Source. Die größten Probleme sind gelöst, aber es gibt immer noch ein paar Unstimmigkeiten.

Olaf bittet um ein Meinungsbild, ob das Thema weiter verfolgt werden soll. Bei 83 von 94 abgegebenen Meinungen stimmen 44 Mitglieder für die Weiterverfolung, 6 dagegen, und 33 enthalten sich.

In Kommentaren bringt die Mitgliedschaft zum Ausdruck, dass die Prioriät bei der Verfügbarkeit von Qt für die Entwicklung von freier Software liegen soll. Olaf erklärt, dass dies in jedem Fall gegeben ist, da es der satzungesgemäße Zweck der KDE Free Qt Foundation ist.

Olaf und Martin bringen den Vorschlag auf, eine Arbeitsgruppe zu bilden, um die Arbeit der Vertreter in der KDE Free Qt Foundation zu unterstützen.

#### Andere Gruppen

Die Financial Working Group besteht aus Mitgliedern die für die Dauer von zwei Jahren gewählt wurden. Diese Dauer ist für einige Mitglieder erreicht. Die Financial Working Group ruft auf, sich als neue Mitglieder der Arbeitsgruppe zu bewerben.

Die Advisory Board Working Group hat zum Zweck, die Beziehung zwischen KDE und seinen Partnern zu pflegen, diesen Ansprechpartner zu bieten. Sie hat dieses Jahr eine Telefon-Konferenz durchgeführt, um die Mitglieder des Advisory-Boards über wichtige Entwicklungen in KDE zu informieren. OpenUK wurde als neues Mitglied des Advisory-Boards aufgenommen.

Die Community Working Group hat ein relativ hohes Aufkommen an Fragen von Menschen, die sich der Community anschließen wollen. Ihr eigentlicher Zweck ist allerdings, bei der Lösung von Konflikten in der Community zu helfen. Es wird angeregt, darüber nachzudenken, den Namen der Arbeitsgruppe anzupassen, um ihre Aufgabe genauer zu reflektieren.

### Wahlen

Es stehen drei Wahlen auf der Tagesordnung. Die Wahlen werden online mit Hilfe des Online-Services [Belenios](https://www.belenios.org) durchgeführt, der sichere, geheime, überprüfbare Wahlen ermöglicht.

Jeff Mitchell, der die technische Betreuung des Wahldienstes übernommen hat, erläutert den Wahlprozess. Die drei Wahlen werden in einem gemeinsamen Vorgang durchgeführt. Nach der Benennung der Kandidaten wird die Kandidaten-Liste geschlossen und es werden per Email die Zugangsdaten zum Wahldienst an alle zu diesem Zeitpunkt anwesenden und stimmberechtigten Mitglieder verschickt. Damit können die Mitglieder ihre Stimmen abgeben und anhand der beigefügten Informationen auch sicher nachvollziehen, dass ihre Stimmen im Wahlergebnis berücksichtigt wurden.

#### Kandidaten

Im Vorstand gibt es drei offene Positionen, da die Amtszeit der entsprechenden Vorstandsmitglieder abgelaufen ist. Alle drei Vorstandsmitglieder stellen sich zur Wiederwahl. Es gibt keine weiteren Kandidaten.

Damit sind die Kandidaten für die Wahl in den Vorstand:

* Aleix Pol Gonzalez
* Eike Hein
* Lydia Pintscher

Als Kassenprüfer stellen sich die Kassenprüfer des letzen Jahres zur Wiederwahl. Es gibt keine weiteren Kandidaten.

Damit sind die Kandidaten für die Wahl zum Kassenprüfer:

* Andreas Cord-Landwehr
* Ingo Klöcker

Für die Vertreter des KDE e.V. in der KDE Free Qt Foundation stellen sich die bisherigen Vertreter zur Wiederwahl. Es gibt keine weiteren Kandidaten.

Damit sind die Kandidaten für die Wahl zum Verteter des KDE e.V. in der KDE Free Qt Foundation:

* Martin Konold
* Olaf Schmidt-Wischhöfer

#### Wahlergebnisse

Es werden 89 elektronische Stimmzettel abgegeben.

Ergebnis Vorstandswahl:

* Aleix Pol Gonzalez: 89 Stimmen
* Eike Hein: 88 Stimmen
* Lydia Pintscher: 86 Stimmen

Damit sind die drei Kandidaten in den Vorstand wiedergewählt. Alle drei Kandidaten erklären, dass sie die Wahl annehmen.

Ergebnis der Wahl der Kassenprüfer:

* Andreas Cord-Landwehr: 88 Stimmen
* Ingo Klöcker: 86 Stimmen

Damit sind die beiden Kandidaten als Kassenprüfer wiedergewählt. Beide Kandidaten erklären, dass sie die Wahl annehmen.

Ergebnis der Wahl der Verteter des KDE e.V. in der KDE Free Qt Foundation:

* Martin Konold: 89
* Olaf Schmidt-Wischhöfer: 84

Damit sind die beiden Kandidaten als Vertreter von KDE e.V. in der KDE Free Qt Foundation wiedergewählt. Beide Kandidaten erklären, dass sie die Wahl annehmen.

Wegen eines Fehlers in der Administration der Wählerliste haben vier anwesende Mitglieder keinen elektronischen Stimmzettel erhalten. Alle anderen anwesenden stimmberechtigten Mitglieder haben ihre Stimme abgegeben. Die betroffenen Mitglieder erklären jeweils, dass sie keine Einwände gegen die Wahl haben. Daraufhin wird von einer Wiederholung der Wahl Abstand genommen und das Ergebnis ist damit gültig.

### Verschiedenes

Es findet eine Diskussion über das Vorgehen im Kontext der KDE Free Qt Foundation statt.

Eine Abfrage des Meinungsbilds zu einer Arbeitsgruppe zur Unterstützung der KDE e.V. Vertreter in der KDE Free Qt Foundation ergibt: 45 Stimmen für die Enrichtung einer Working Group, 9 Gegenstimmen, keine Enthaltungen.

### Schluss der Versammlung

Um 17:30 schließt der Versammlungsleiter die Mitgliederversammlung.

Die Versammlung fand frei von Ton- oder Videounterbrechungen statt.
