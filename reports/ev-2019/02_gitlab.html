<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;"><img src="images/GitLab/commit-brooklyn-graffiti-cover.jpg" width="100%" /></figure>
</div>


<p>One of the many ambitious things KDE set out to do in 2019 was to work on transitioning to GitLab. The frameworks that have helped KDE contributors manage their work for years have become a bit creaky.  The job sysadmins and developers carried out to create and maintain the set up is beyond reproach of course, but 25 years of tweaks and adjustments are going to lead to some level of cruft that can become an issue.</p>

<p>The infrastructures we have used up until now also use old technologies which a lot of the younger developers approaching KDE today are unfamiliar with. The first rule of the Usability Club is "don't push hurdles in the users' way". Unfamiliar custom frameworks are hurdles to new contributors and making development for KDE more accessible to a new generation was key when the Community decided to adopt GitLab. GitLab offers interfaces and workflows that are familiar to most developers from its own and other popular online services.</p>

<p>There is also the fact that GitLab comes with the technical benefits of providing a version control system with continuous integration and deployment, issue tracking and workboards, something that any project leader will appreciate.</p>

<p>However, a legacy system of decades that serves a community of thousands of active developers is going to take some work moving away from. That it only took 9 months to move past phase 1 should be considered a feat.</p>

<p>GitLab, inc. appreciated that and, although we were only halfway through phase 1 at the time, in September they invited us to their first <I>Commit</I> event and then invited us again in October to the second edition so we could share our insight and help other communities and enterprises by explaining how the move was going and warn of the stumbling blocks they would encounter along the way.</p>

<h3>GitLab Commit Brooklyn</h3>

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;"><a href="images/GitLab/Brooklyn.jpg"><img src="images/GitLab/Brooklyn.jpg" width="800" /></a><br /><figcaption>Sid Sijbrandij, CEO at GitLab, addresses atendees at GitLab Commit Brooklyn.</figcaption></figure>
</div>

<p><a href="https://about.gitlab.com/events/commit/brooklyn/">GitLab Commit Brooklyn</a> was held on the 17th of September in the trendy borough of Brooklyn. We were invited to talk on a panel called <a href="https://gitlabcommit2019brooklyn.sched.com/event/TgZ3/how-ginormous-orgs-are-going-cloud-native-hint-they-are-not-companies">"How Ginormous Orgs Leverage GitLab (hint they are not companies)"</a>, because, yes, that's what KDE is: with over 2,600 developer accounts, and 5,000 commits per month, we overshadow in volume of development giants like Delta Airlines, Goldman Sachs and T-Mobile, all of which were also attending the meeting.</p>

<p>On the panel, apart from myself, was George Tsiolis, representing the community of independent GitLab developers; Molly de Blanc, Strategic Initiatives Manager at the GNOME Foundation; and Ray Paik, Community Manager at GitLab and the moderator of the panel. </p>

<p>During the talk, I found myself saying "what Molly said" a lot, since the reasons to adopt GitLab and the experience GNOME had lived through were essentially the same to what KDE itself was going through at the time. She mentioned how the old framework had become somewhat unwieldy, how GitLab offered an integrated interface with many of the services contributors required, and how a customized platform, although tailor-made to suit many developers needs, was a barrier to many newcomers, something that GitLab was not. Tick, tick, tick.</p>

<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/B5JAZclKG3E" allowfullscreen></iframe>
</div>

<a href="https://digitalanarchist.com/videos/gitlab-commit-brooklyn-september-17-2019/paul-brown-kde-gitlab-commit-brooklyn-2019">I also got interviewed on the Digital Anarchist by veteran vlogger Alan Shimel</a>. This gave me the chance to explain to his viewers what KDE is and what we have to offer. This may have been more interesting than it seems at first glance, since, outside the people I shared the panel with, I only met one person who knew what KDE was: Eddie Zaneski from <a href="https://www.digitalocean.com/">Digital Ocean</a>. Eddie also admitted to being a user.


<p>I am not sure if this ignorance was a US thing or a that-particular-crowd thing. While it is true that KDE seems to be more popular in Europe (and Brasil and India) than in the US, the number of MacBooks on display made me think that attendees were not especially familiar with GNOME or, indeed, any graphical environment that was not macOS either; that Free Software, in general, was only something most attendees used for hosting and running cloud applications and services and not something they really interacted with directly.</p>

<p>So talking from Shimel's podium, supposing it reaches the same crowd, may have at least made some of his audience aware of KDE's existence and that such things as Free Software applications for end users do really exist.</p>

<h3>GitLab Commit London</h3>

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;"><a href="images/GitLab/London.jpg"><img src="images/GitLab/London.jpg" width="800" /></a><br /><figcaption>Attendees at GitLab Commit London.</figcaption></figure>
</div>

<p>I did not encounter the same lack of awareness in London. But, then again, comparing one crowd with another would be like comparing apples and oranges. At <a href="https://about.gitlab.com/blog/2019/10/09/live-from-commit-london/">GitLab Commit London</a> (October 9, 2019), the crowd was made up by mostly Europeans, but they also came from smaller outfits. While a large percentage of the audience in Brooklyn were attending from massive corporate multinationals, at the event set in the heart of The City, London's financial district, there were more attendees from SMEs.</p>

<p>This makes sense: most of the European IT industry is made up of companies with less than 250 employees. Are developers that work for smaller enterprises more knowledgeable of what FLOSS is and the names of the communities that power it? It would seem so, even if you just judged the crowd by the stickers on their laptops. Without reaching the extremes of nerdiness you would find at, for example, FOSDEM, it was a refreshing change from the silver sea of brushed metal MacBooks I had witnessed at Brooklyn. This also translated into the talks, that were in general quite less buzzword-heavy.</p>

<p>Either way, again I was on a panel, we all talked about how big organisations like KDE, GNOME and, in this edition, <a href="https://Freedesktop.org">Freedesktop</a> adopted GitLab; and again the genial Ray Paik hosted. This time, apart from Ray, I was accompanied by Carlos Soriano, GNOME Board director who works on newcomers experience; and Daniel Stone, Project Administrator at Freedesktop.</p>

<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/fAGHBLmV7OA" allowfullscreen></iframe>
</div>

Carlos, Daniel and I had plenty of time to talk beforehand and exchange ideas on how we could work together more. Carlos and I also recorded a video together for GitLab's marketing team, promoting Linux desktop environments to the GitLab community.

In view of how little people outside our bubble, even technical people, know about Linux-native end-user software, maybe we should be doing more of that.
